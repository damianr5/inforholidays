import { Component } from '@angular/core';


import { UsaPage } from '../usa/usa';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = UsaPage;

  constructor() {

  }
}
