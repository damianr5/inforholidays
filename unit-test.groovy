<!DOCTYPE html>
<head>
    <title>
        Build error report
    </title>
    
    <style>
        body, table, td, th{
          font-family:Verdana,Helvetica,sans serif;
          font-size:11px;
          color:black;
          border-style:none !important;
        }
        td.headers { 
          color:white; 
          background-color:#0000C0; 
          font-size:120%; 
          text-align:center;
        }
        td.names{
          text-align:right;
        }
        td.console { 
          font-family:Courier New; 
        }
        table.logs{
          width:100%;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
            </td>
            <td>
                <b style="font-size:200%; color:red; text-align:center">
                    BUILD ${build.result}
                </b>
            </td>
        </tr>
        <br/>
        <tr>
            <td class="names">
                Project :
            </td>
            <td>
                ${project.name}
            </td>
        </tr>
        <tr>
            <td class="names">
                Date :
            </td>
            <td>
                ${it.timestampString}
            </td>
        </tr>
        <tr>
            <td class="names">
                Duration :
            </td>
            <td>
                ${build.durationString}
            </td>
        </tr>
    </table>
    <br/>


    <!-- COMMIT LOG -->
    <% if(build.result==hudson.model.Result.FAILURE) { 
      def checkLog = build.getLog(1000) %>
        <table class="logs">
            <tr>
                <td class="headers">
                    <b>
                        LAST COMMIT
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <br/>

            <% boolean flag = false
               checkLog.any { element ->
                 if (element == "Last Commit Details:" || flag) {
                   flag = true 
                   if (element == "gitEnd") {
                     flag = false 
                     return true
                   } %>
            <tr>
                <td class="console">
                    ${org.apache.commons.lang.StringEscapeUtils.escapeHtml(element)}
                </td>
            </tr>
                 <% }
               }
    } %>
            
            <br/>
            <br/>
            <tr>
                <td class="headers">
                    <b>
                        CHECK THE ATTACHMENT FOR A BETTER VIEW
                    </b>
                </td>
            </tr>
        </table>
</body>