import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { _appIdRandomProviderFactory } from '../../../node_modules/@angular/core/src/application_tokens';
import { importType } from '../../../node_modules/@angular/compiler/src/output/output_ast';

@Component({
  selector: 'page-usa',
  templateUrl: 'usa.html'
})
export class UsaPage {
  
  holidays: any[];
  
    
  constructor(public navCtrl: NavController, private http: Http) {
    let localData = this.http.get('assets/US_holidays.json').map(res => res.json().holiday);
    localData.subscribe(data => {
      this.holidays = data;
    });
  }

  toggleSection(i) {
    this.holidays[i].open = !this.holidays[i].open;
  }

  removeDupl(holidays){
    var cleaned = [];
    for(var i =0;i<holidays.length; i++){
      for(var j =i+1; j<holidays.length; j++){
        if(holidays.name[i] === holidays.name[j]){
          console.log (cleaned.push(holidays[i]));
          console.log('test');
        }
      
      }
    }
  }


}
